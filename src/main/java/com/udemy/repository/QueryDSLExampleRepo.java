package com.udemy.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import com.udemy.entity.Course;
import com.udemy.entity.QCourse;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository("queryDSLExampleRepo")
public class QueryDSLExampleRepo {

//    En todos los repositorios que usen QueryDCL
//    necesitamos declarar las clases Q que nos han generado el plugin

//	NOTA: Si no encuentra la Qclase es porque la clase no pertenece a los recursos del proyecto
//	mvn eclipse:eclipse
//	mvn idea:idea

    private QCourse qCourse = QCourse.course;

//    EntitiManager objeto que se encarga de gestionar las entidades de la persistencia de la aplicacion
    @PersistenceContext
    private EntityManager em;

//    Metodo de ejemplo para practicar
    public Course find(Boolean exist){

        JPAQuery<Course> wuery = new JPAQuery<Course>(em);

        BooleanBuilder predicaBuilder = new BooleanBuilder(qCourse.description.endsWith("OP"));

        if (exist){
            Predicate predicate2 = qCourse.id.eq(23);
            predicaBuilder.and(predicate2);
        }else{
            Predicate predicate3 = qCourse.name.endsWith("OP");
            predicaBuilder.or(predicaBuilder);
        }

        return wuery.select(qCourse).from(qCourse).where(predicaBuilder).fetchOne();
    }

}
