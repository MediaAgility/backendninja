package com.udemy.repository;

import com.udemy.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("courseJpaRepository")
public interface CourseJpaRepository extends JpaRepository<Course, Serializable> {
//    Van las nuevas consultas que agregemos
//    public abstract Course findByPrice(int price);
}
