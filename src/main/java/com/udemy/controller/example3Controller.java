package com.udemy.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.udemy.model.Person;

import javax.validation.Valid;

@Controller
@RequestMapping("/example3")
public class example3Controller {
	
	private static final Log LOGGER = LogFactory.getLog(example3Controller.class);
	
	public final static String FINAL_VIEW = "form";
	public final static String RESULT_VIEW = "result";
	
//	FORMA UNO PARA REDIRIGIR
//	@GetMapping("/")
//	public String redirect() {
//		return "redirect:/example3/showform";
//	}
	
//	FORMA DOS PARA REDIRIGIR
	@GetMapping("/")
	public RedirectView redirect() {
		return new RedirectView("/example3/showform");
	}
	
	@GetMapping("/showform")
	public String showForm(Model model) {
		LOGGER.info("Info Trace");
		LOGGER.warn("Warning Trace");
		LOGGER.error("error Trace");
		LOGGER.debug("Debug Trace");
		model.addAttribute("person",new Person());
		return FINAL_VIEW;
	}
	
	@PostMapping("/addperson")
	public ModelAndView addPerson(@Valid @ModelAttribute("person") Person person, BindingResult bindingResult) {
		ModelAndView mav = new ModelAndView();
		if (bindingResult.hasErrors()){
			mav.setViewName(FINAL_VIEW);
			LOGGER.info("METHOD: 'addperson' --ERROR EN LA VALIDACION: ' " + bindingResult + "'");
		}else {
			mav.setViewName(RESULT_VIEW);
			mav.addObject("person", person);
			LOGGER.info("METHOD: 'addperson' --PARAMS: ' " + person + "'");
		}

		return mav;
	}

}
