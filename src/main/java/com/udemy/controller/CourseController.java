package com.udemy.controller;

import com.udemy.entity.Course;
import com.udemy.service.CourseService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/course")
public class CourseController {

    //    Vistas
    private static final String COURSES_VIEW = "course";

    //    Inyeccion de el servico CurseService
    private final CourseService courseService;

    public CourseController(@Qualifier("courseServiceImpl") CourseService courseService) {
        this.courseService = courseService;
    }

    //    LOGS
    private static final Log LOG = LogFactory.getLog(CourseController.class);

    @GetMapping("/listcourses")
    public ModelAndView listAllCourses() {
        LOG.info("call: --listAllCourses()-- ");
        ModelAndView mav = new ModelAndView(COURSES_VIEW);
        mav.addObject("courses", courseService.listAllcourses());
        mav.addObject("course", new Course());
        return mav;
    }

    @PostMapping("/addcourse")
    public String addCourse(@ModelAttribute("course") Course course) {
        LOG.info("call: -- addCourse() --PARAMS: " + course.toString() + " --");
        courseService.addCourse(course);
        return "redirect:/course/listcourses";
    }

}
