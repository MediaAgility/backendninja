package com.udemy.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.component.ExampleComponent;
import com.udemy.service.ExampleService;

@Controller
@RequestMapping("/say")
public class helloController {

    public static final String VIEW = "example";

//	INYECTANDO UN COMPONENTE "ExampleComponent" Y UN SERVICIO "Exampleservice"

//	FORMA DE ESTE CURSO
//	@Autowired
//	@Qualifier("exampleComponent")
//	private ExampleComponent exampleComponent;


//	FORMA CON CONSTRUCTOR
//	PARA NO USAR EL AUTOWIRED SE CREAN LAS VARIABLES FINALES Y SE INICIALIZAN EN UN MISMO CONSTRUCTOR

    private final ExampleComponent exampleComponent;
    private final ExampleService exampleservice;

    public helloController(@Qualifier("exampleComponent") ExampleComponent exampleComponent,
                           @Qualifier("exampleservice") ExampleService exampleservice) {
        this.exampleComponent = exampleComponent;
        this.exampleservice = exampleservice;
    }

//	CONTROLADORES

    //	Primer Forma
    @GetMapping("/hello")
    public String hello(Model model) {
        exampleComponent.sayHello();
        model.addAttribute("people", exampleservice.getListPeople());
        return VIEW;
    }

    //	Segunda Forma
    @GetMapping("/mav")
    public ModelAndView modelandview() {
        ModelAndView mav = new ModelAndView(VIEW);
        mav.addObject("people", exampleservice.getListPeople());
        return mav;
    }

}
