package com.udemy.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.udemy.model.Person;
import com.udemy.service.ExampleService;

@Service("exampleservice")
public class ExampleServiceImpl implements ExampleService {

//	LOG
	private static final Log LOG = LogFactory.getLog(ExampleServiceImpl.class);

	@Override
	public List<Person> getListPeople() {
		List<Person> people = new ArrayList<>();
		people.add(new Person("Janet", 25));
		people.add(new Person("Pedro", 25));
		people.add(new Person("PAblo", 25));
		people.add(new Person("Bilma", 25));
		people.add(new Person("Carmen", 25));
		LOG.info("DESDE EXAMPLE SERVICE IMPL");
		return people;
	}
}
