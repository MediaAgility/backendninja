package com.udemy.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.udemy.component.RequestTimeInterceptor;

@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {
	
//	INYECCION COMO EN EL EJEMPLO DEL CURSO
//	@Autowired
//	@Qualifier("requestTimeInterceptor")	
//	private RequestTimeInterceptor requestTimeInterceptor;
	
//	FORMA DE INYECTAR COMO SE DEBERIA
	@Qualifier("requestTimeInterceptor")	
	private final RequestTimeInterceptor requestTimeInterceptor;
	
	public WebMvcConfiguration(RequestTimeInterceptor requestTimeInterceptor) {
		this.requestTimeInterceptor = requestTimeInterceptor;
	}
	
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requestTimeInterceptor);
	}
	
	

}
